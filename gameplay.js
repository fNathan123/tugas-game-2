
let main = new Phaser.Scene('main');
main.preload=function(){
    this.load.setBaseURL('assets');
    this.load.image('box','box.png');
    this.load.atlas('pohon','tile2.png','pohon.json')
    this.load.atlas('character','spritesheet.png','spritesheet.json');
    this.load.atlas('invisObj','tile2.png','invis.json');
    this.load.atlas('terrain','tile2.png','terrain.json');
    this.load.image('apel','apple.png');
}
var platform;
var jump=true;
var doubleJump=true;
var runner;
var queueBox=[];
var boxidx=0;
var velo=-250;
var speed=10;
var scoreText;
var score=0;

main.create=function(){    
    runner=this.physics.add.sprite(config.width/2,config.height/2,'character','RunRight01.png');
    platforms = this.physics.add.staticGroup();
    this.terrains = this.add.group();
    this.trees=this.add.group();
    this.boxes=this.add.group();
    this.fruits=this.add.group();
    
    for(i=0;i<10;i++){
        this.generateTerrain((110*i),config.height/2);
    }
    
    this.generatePlatform(config.width/2,config.height/2);
    

    runner.body.setGravityY(1000);
    runner.setDepth(1);

    this.anims.create(
        {key:'running',
        frames:this.anims.generateFrameNames('character',{start:1,end:4,zeroPad:2,prefix:'RunRight',suffix:'.png'}),
        repeat:-1,
        frameRate:10
        }
    );

    this.anims.create(
        {key:'jump',
        frames:this.anims.generateFrameNames('character',{start:1,end:1,zeroPad:2,prefix:'RunRight',suffix:'.png'}),
        repeat:0,
        frameRate:10
        }
    );

    this.input.keyboard.on('keydown_SPACE', ()=> {
        if(jump || doubleJump){
            if(!jump){
                console.log(jump+" "+doubleJump);
                doubleJump=false;
            }
            runner.play('jump');
            runner.setVelocity(0,-600);
            jump=false;

        }
    });
    this.timer=this.time.addEvent({
        delay:1500,
        callback:()=>{
            velo+=-2;
            var jumlah=Math.floor(Math.random() * 5) ;
            //boxidx+=jumlah;
            if(jumlah>0)queueBox.push(jumlah);
            for (var i = 0; i < jumlah; i++)
                this.addOneBox(800, (config.height/2-(i * 40))+100);
                        
            },
        callbackScope:this,
        loop:true
    });
    this.physics.add.collider(runner,platforms,()=>{
        doubleJump=true;
        jump=true;
        
        
    });
    this.physics.add.collider(runner,this.boxes,()=>{
        this.restart();
        
    });
    this.physics.add.overlap(runner,this.fruits,addition,null,this);
    runner.play('running');
    scoreText = this.add.text(16, 16, 'score: 0', { fontSize: '32px', fill: '#fff' });
}
var index=0;
var fIdx=0;


main.update=function(){
    //console.log(doubleJump);
    if(this.terrains.getLength()>index && this.terrains.getChildren()[index].x<0){
        //console.log('msk');
        index+=1;
        this.generateTerrain(10*100,config.height/2);
    }
    if(!jump){
        runner.play('running');
    }
    if(this.boxes.getLength()>0){
        if(this.boxes.getChildren()[boxidx]!=null && this.boxes.getChildren()[boxidx].x<runner.x){
            console.log("pass "+queueBox);
            var q=queueBox.shift();
            boxidx+=q;
            score+=q;
            scoreText.setText("score: "+score);
            //queueBox.shift();
            
        }
    }
    
}

function addition(runner,fruit){
    fruit.disableBody(true,true);
    score++;
    scoreText.setText("score: "+score);
}

main.generatePlatform=function(x,y){
    platforms.create(x,y+130,'invisObj');
}

main.generatePohon=function(x,y){
    var pohon=this.physics.add.sprite(x,y,'pohon');
    pohon.setActive();
    pohon.setVelocity(velo,0);
    pohon.setGravity(0);
    pohon.setScale(2);
    var isFruit=Math.floor(Math.random() * 10);
    
    //console.log(isPohon);
    if(isFruit>5)this.generateFruit(x,y);
}
main.generateFruit=function(x,y){
    var fruit=this.physics.add.sprite(x,y-50,'apel');
    fruit.setActive();
    fruit.setVelocity(velo,0);
    fruit.setGravity(0);
    fruit.setScale(0.05);
    this.fruits.add(fruit);
}
main.generateTerrain=function(x,y){
    var terrain=this.physics.add.sprite(x,y+227,'terrain');
    terrain.setActive();
    terrain.setVelocity(velo,0);
    terrain.setGravity(0);
    this.terrains.add(terrain);
    terrain.setScale(2);
    var isPohon=Math.floor(Math.random() * 10);
    //console.log(isPohon);
    if(isPohon>6)this.generatePohon(x,y);
}

main.addOneBox=function(x,y){
    var box = this.physics.add.image(x,y,'box');
    box.setActive();
    box.setVelocity(velo,0);
    box.setGravity(0);
    box.setScale(0.7);
    this.boxes.add(box);
}
main.restart=function(){
    index=0;
    boxidx=0;
    score=0;
    velo=-250;
    scoreText.setText('score :0');
    this.scene.start('restartMenu');
}
