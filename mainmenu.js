let menu = new Phaser.Scene('menu');
menu.preload=function preload ()
{
    this.load.setBaseURL('assets');
    this.load.image('kotak kuning','start.png');
}

menu.create=function create ()
{
    var btn = this.add.image(config.width/2,config.height,'kotak kuning');
    btn.scaleX=0;
    btn.scaleY=0;
    btn.setInteractive();
    btn.on('pointerup',function(){
        this.scene.scene.start('main');
    });


    this.tweens.add({
        targets:[btn],
        y:config.height/2,
        duration:500,
        scaleX:1,
        scaleY:1,
        ease: 'Bounce.easeOut'
    });
}
